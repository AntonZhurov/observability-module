# Traicing

## Библиотеки
- `@opentelemetry/api` - главная библиотека, через нее вызываем весь используемый функционал opentelemetry
- `@opentelemetry/exporter-trace-otlp-http` - экспортирует наши данные в коллектор
- `@opentelemetry/resources` - позволяет добавлять информацию о нашем окружении
- `@opentelemetry/sdk-node` - SDK для NodeJs, он может самостоятельно определить некоторые из системных ресурсов. Так же при помощи данного sdk можно добавлять автоинструменты opentelemetry, при необходимости
- `@opentelemetry/sdk-trace-base` - библиотека для активации трейсинга
- `@opentelemetry/semantic-conventions` - набор констант, которые определяют имена ресурсов в соотвествии с соглашением opentelementry


Материалы для подробного ознакомления
- [Подробней про Opentelemetry](https://opentelemetry.io/docs/)
- [Подробней про NodeSDK](https://opentelemetry.io/docs/languages/js/)
- [Подробней про Node auto-instrumentation](https://github.com/open-telemetry/opentelemetry-js-contrib/tree/main/metapackages/auto-instrumentations-node#supported-instrumentations)

## Описание

Два  инструмента для трейсинга находятся в файле `/trace/utils.ts`.
Внутри две функции `span()` и `trace()`

```ts
async function span<TReturn>(
  name: string,
  cb: (span: opentelemetry.Span) => Promise<TReturn> | TReturn,
): Promise<TReturn>;


export async function trace<TReturn>(
  name: string,
  cb: (span: opentelemetry.Span) => Promise<TReturn> | TReturn,
): Promise<TReturn>
```

Различие только в том, что `trace()` запускает контекст ObservabilityStorage с контекстом 'custom'. Утилита нужна для того, что можно было трейсить код, который запускается не запросом к api (запросы к api трейсятся автоматически, если вы не отключили эту возможность). К примеру при помощи `trace()` можно затрейсить код, который запустил kafkaConsumer, RabbitMqConsumer или BullMqWorker. Первым параметром передаем имя трейса, вторым параметром передаем колбек, который будет обвернут в спан. 

В случае ошибки в спан будет записана ошибка.

В колбек передается активный спан для случая если вам необходимо добавить в span ивенты или какие либо дополнительные ресурсы, которые помогут вам лучше разобраться в том, что произошло на самом деле.

Так же вы можете добавить дополнительные параметры в утилиты, или написать дополнительные, если вам нужно добавить кастомную логику.

Все спаны, которые будут созданы внутри колбека будут записаны как дочерние. Создавать дочерные спаны нужно так же при помощи утилиты `span()`.

Использование `trace()` внутри `trace()` приведет к тому, что будут созданы два отдельных трейса с разным traceId

## Экспорт трейсов
Тресы экспортяться автоматически, в файле `setup` создается `TraceExporter`
```ts
const traceExporter = new OTLPTraceExporter({
    url: `${traceExportEndpoint}/v1/traces`,
  });
```
Если вы также собираетесь экспортировать ваши трейсы используя http OTLP, то вам ничего не нужно менять, только настроить доступность эндоинта для вашего бекенда. Если вы хотите использовать другой формат экспорта, то обратитесь к [документации OTLP]((https://opentelemetry.io/docs/)).