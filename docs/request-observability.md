# Request observability

В этой фитче объеденяются все подмодули.
В процессе каждого запроса создается трейсинг, все логи которые пишутся в рамках трейса получают поле traceId, а модуль метрик замеряет время ответа.
Как говорилось разделе про установку все будет работать только на fastify адаптере.

## Как это работает

Когда вы устанавливаете `ObservabilityModule` в его конструкторе вызывается функция `setupObservability()`

```ts
function setupObservability(
  httpAdapter: AbstractHttpAdapter,
  logger: Logger,
  metrics: MetricsService,
  config: ObservabilityModuleConfig,
) {
  switch (httpAdapter.getType()) {
    case 'fastify':
      return setupFastifyObservability(httpAdapter as FastifyAdapter, logger, metrics, config);
    default:
      return;
  }
}
```

После чего в функции `setupFastifyObservability()` устанавливается 2 хука на fastify

```ts
function setupFastifyObservability(
  httpAdapter: FastifyAdapter,
  logger: Logger,
  metrics: MetricsService,
  config: ObservabilityModuleConfig,
) {
  httpAdapter
    .getInstance()
    .addHook('onRequest', createOnRequestHook(logger, metrics, config))
    .addHook('onResponse', createOnResponseHook());
}
```

`onRequestHook` отвечает за:
- Проверяет не внесен ли эндпоинт в black list, если да, то трейсинг не будешь запущен
- Проверяет, что в объекте request установлена переменная `startTime`. Она нужна для того чтобы в лог вывести время ответа
- Забирает парматеризированный путь до хендлера, что позволяет собирать метрики на эндпоинт, а не url
- Запускает таймер для метрики "время ответа"
- Создает спан для запроса и инициализирует AsyncLocalStorage для запроса, у инициализации есть 2 стратегии - `http` и `graphql`

Если url запроса совпадает со значением `graphql: { url: string }` из конфигурации модуля, то AsyncStorage будет иметь контекст `graphql`, в противном случае `http`.

`onResponseHook` отвечает за:
- Завершает спан запроса
- Завершает таймер сбора метрики "время ответа" 
- Логирует ответ. Логирование зависит от типа контекста AsyncStorage



## ObservabilityContext
Любой из форматов трейсинга запускает `ObservabilityStorage`, для каждого из вариантов существует свой контекст. На запросы они создаются при помощи утилиты `/observability/utils/observability-store.factory.ts`. Так как контексты для `graphql` и `http` отличаются только одним полем, то в утилите определяется только тип контекста.

## Http Context
Стандартный контекст для приложения.
```ts
interface HttpRequestObservabilityContext {
  type: 'http';
  req: http.IncomingMessage;
  res: http.ServerResponse;
  route: FastifyRequest['routeOptions'];
  logger: Logger;
  requestSpan: Span;
  collectResponseTimeCb: ReturnType<MetricsService['responseTimeCollector']['startTimer']>;
  error?: Error;
}
```
Вся информация для данного контекста собирается сразу же, как fastify вызвал `onRequestHook`, после чего дополнять его нету необходимости, если только вы хотите добавить в контекст ошибку.

## Graphql Context
Создается при условии совпадения url запроса с url указаным в настройках модуля.
```ts
interface GraphQlRequestObservabilityContext {
  type: 'graphql';
  req: http.IncomingMessage;
  res: http.ServerResponse;
  route: FastifyRequest['routeOptions'];
  logger: Logger;
  requestSpan: Span;
  graphqlOperationDetails?: GraphQlOperationDetails;
  collectResponseTimeCb: ReturnType<MetricsService['responseTimeCollector']['startTimer']>;
  error?: Error;
}
```

Единственное отличие это поле `graphqlOperationDetails`. В это поле сохраняется подробная информация об обработчике запроса. Оно опциональное потому, что получить его значения можно только после резолвинга Apollo Server. Для этого и добавляется плагин в Apollo. Когда плагин добавлен, то после того как ApolloServer закончит резолвинг и приступит к обработке запроса, контекст `ObservabilityStorage` будет обновлен и в него будет добавлена информация в поле `graphqlOperationDetails`. Так же будет обновлено имя для спана запроса, чтобы оно соответсвовало обработчику.
В завершении запроса в лог будет добавлена информация от apollo.

## Стратегии завершения запроса

Применение стратегий описанны в файле `/observability/utils/response-handler.ts`. Стратегия для каждого типа контекста описывается в самой папке типа. При необходимости вы можете обогатить/обеднить логи или добавить доп информацию в спан запроса/метрику "время ответа".