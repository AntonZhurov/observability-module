import { HookHandlerDoneFunction } from 'fastify';

import { OnResponseGraphQlHook } from '../graphql/on-response.hook';
import { OnResponseHttpHook } from '../http/on-response.hook';
import { ObservabilityStore, ObservabilityStoreContextType } from '../observability.storage';

export function HandleResponse(store: ObservabilityStore, done: HookHandlerDoneFunction) {
  try {
    const cb = Strategies[store.context.type];
    cb(store);
  } catch (e) {
  } finally {
    done();
  }
}

export type OnResponseHookCb = (store: ObservabilityStore) => void;

const Strategies: Record<ObservabilityStoreContextType, OnResponseHookCb> = {
  http: OnResponseHttpHook,
  graphql: OnResponseGraphQlHook,
  custom: () => {},
};
