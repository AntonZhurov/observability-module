import { Span } from '@opentelemetry/api';
import { FastifyReply, FastifyRequest } from 'fastify';

import { Logger } from '../logger';
import { ObservabilityModuleConfig } from '../observability.config.type';
import { ObservabilityStore, ObservabilityStoreContextType } from '../observability.storage';

interface Options {
  reply: FastifyReply;
  request: FastifyRequest;
  logger: Logger;
  span: Span;
  collectResponseTimeCb: any;
}

function getRequestType(config: ObservabilityModuleConfig, request: FastifyRequest): ObservabilityStoreContextType {
  if (config.graphql?.url) {
    if (request.routeOptions.url === config.graphql.url) {
      return 'graphql';
    }
  }

  return 'http';
}

export function createObservabilityStore(
  config: ObservabilityModuleConfig,
  { request, reply, logger, span, collectResponseTimeCb }: Options,
): ObservabilityStore {
  const routeOptions = request.routeOptions;

  return {
    traceId: span.spanContext().traceId,
    context: {
      type: getRequestType(config, request),
      req: request.raw,
      res: reply.raw,
      collectResponseTimeCb: collectResponseTimeCb,
      logger: logger,
      requestSpan: span,
      route: routeOptions,
    },
  };
}
