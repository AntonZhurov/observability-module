import * as http from 'node:http';

import { HttpRequestObservabilityContext } from './observability.context';

interface BuildPayload {
  req: http.IncomingMessage;
  res: http.ServerResponse;
  err?: Error;
  responseTime?: number;
}

const buildSuccessfulLog = ({ req, res, responseTime }: BuildPayload) => ({
  msg: 'Request completed',
  req: {
    id: req['id'],
    url: req.url,
    method: req.method,
    headers: req.headers,
  },
  res: {
    status: res.statusCode,
    headers: res.getHeaders(),
  },
  responseTime: responseTime,
});

const buildFailedLog = ({ req, res, err, responseTime }: BuildPayload) => ({
  msg: 'Request failed',
  req: {
    id: req['id'],
    url: req.url,
    method: req.method,
    headers: req.headers,
  },
  res: {
    status: res.statusCode,
    headers: res.getHeaders(),
  },
  err: err,
  responseTime: responseTime,
});

export const logHttpResponse = (context: HttpRequestObservabilityContext, err?: any) => {
  const { res, req, logger, error } = context;
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  //@ts-ignore
  const responseTime = Date.now() - res['startTime'];
  if (error) {
    logger.error(buildFailedLog({ req: req, res: res, err: error ?? err, responseTime }));
  } else {
    logger.log(buildSuccessfulLog({ req: req, res: res, responseTime }));
  }
};
