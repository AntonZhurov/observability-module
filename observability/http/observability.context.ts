import * as http from 'node:http';

import { Span } from '@opentelemetry/api';
import { FastifyRequest } from 'fastify';

import { Logger } from '../logger';
import { MetricsService } from '../metrics/metrics.service';

export interface HttpRequestObservabilityContext {
  type: 'http';
  req: http.IncomingMessage;
  res: http.ServerResponse;
  route: FastifyRequest['routeOptions'];
  logger: Logger;
  requestSpan: Span;
  collectResponseTimeCb: ReturnType<MetricsService['responseTimeCollector']['startTimer']>;
  error?: Error;
}
