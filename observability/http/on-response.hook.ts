import { OnResponseHookCb } from '../utils/response-handler';
import { logHttpResponse } from './response-logger';

export const OnResponseHttpHook: OnResponseHookCb = (store) => {
  if (store.context.type !== 'http') return;

  const { route, res } = store.context;

  store.context.collectResponseTimeCb({
    method: route.method,
    path: route.url,
    status: res.statusCode,
  });
  store.context.requestSpan.end();

  logHttpResponse(store.context);
};
