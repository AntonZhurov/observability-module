import { AbstractHttpAdapter } from '@nestjs/core';
import { FastifyAdapter } from '@nestjs/platform-fastify';

import { setupFastifyObservability } from './fastify/setup';
import { Logger } from './logger';
import { MetricsService } from './metrics/metrics.service';
import { ObservabilityModuleConfig } from './observability.config.type';

export function setupObservability(
  httpAdapter: AbstractHttpAdapter,
  logger: Logger,
  metrics: MetricsService,
  config: ObservabilityModuleConfig,
) {
  switch (httpAdapter.getType()) {
    case 'fastify':
      return setupFastifyObservability(httpAdapter as FastifyAdapter, logger, metrics, config);
    default:
      return;
  }
}
