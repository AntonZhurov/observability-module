import { DynamicModule, FactoryProvider, Global, Module, Provider } from '@nestjs/common';

import { Logger } from './logger';
import { PinoLoggerProvider } from './pino/pino.logger';
import { LOGGER_MODULE_OPTIONS_TOKEN, LoggerModuleAsyncOptions, LoggerModuleOptions } from './types';

const defaultOptions: LoggerModuleOptions = {
  logLevel: 'info',
  isDevelopment: false,
};

@Global()
@Module({
  providers: [
    {
      provide: LOGGER_MODULE_OPTIONS_TOKEN,
      useValue: defaultOptions,
    },
    Logger,
    PinoLoggerProvider,
  ],
  exports: [Logger],
})
export class LoggerModule {
  static forRoot(options: LoggerModuleOptions = defaultOptions): DynamicModule {
    const optionsProvider: Provider<LoggerModuleOptions> = {
      provide: LOGGER_MODULE_OPTIONS_TOKEN,
      useValue: options,
    };

    return {
      module: LoggerModule,
      global: true,
      providers: [optionsProvider, Logger, PinoLoggerProvider],
      exports: [Logger],
    };
  }

  static forRootAsync(options: LoggerModuleAsyncOptions): DynamicModule {
    const optionsProvider: FactoryProvider<LoggerModuleOptions> = {
      provide: LOGGER_MODULE_OPTIONS_TOKEN,
      useFactory: options.useFactory,
      inject: options.inject,
    };

    return {
      module: LoggerModule,
      global: true,
      providers: [optionsProvider, Logger, PinoLoggerProvider],
      exports: [Logger],
    };
  }
}
