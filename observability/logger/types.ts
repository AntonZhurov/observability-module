import { FactoryProvider } from '@nestjs/common';

export type LoggerLevel = 'fatal' | 'error' | 'warn' | 'info' | 'debug' | 'trace' | 'silent';

export type ValueOf<T extends object> = T[keyof T];

export const LOGGER_MODULE_OPTIONS_TOKEN = 'logger_options';
export const PINO_TOKEN = 'pino_instance';

export interface LoggerModuleOptions {
  isDevelopment?: boolean;
  logLevel?: LoggerLevel;
}

export type LoggerModuleAsyncOptions = Pick<FactoryProvider<LoggerModuleOptions>, 'useFactory' | 'inject'>;
