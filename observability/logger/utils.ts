import { ObservabilityStorage } from '../observability.storage';

export const getTraceId = () => {
  return ObservabilityStorage.getStore()?.traceId;
};
