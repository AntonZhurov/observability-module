import { Provider } from '@nestjs/common';
import pino, { TransportSingleOptions } from 'pino';

import { LoggerOptions } from '../logger';
import { LOGGER_MODULE_OPTIONS_TOKEN, PINO_TOKEN } from '../types';

function createTransport(isDevelopment: boolean) {
  if (!isDevelopment) return;

  return {
    target: 'pino-pretty',
    options: {
      colorize: true,
    },
  } as TransportSingleOptions;
}

function createPino(options: LoggerOptions) {
  return pino({
    level: options.logLevel,
    timestamp: true,
    formatters: {
      level(label) {
        return { level: label };
      },
    },
    transport: createTransport(options.isDevelopment),
  });
}

export const PinoLoggerProvider: Provider = {
  provide: PINO_TOKEN,
  useFactory: (options: LoggerOptions) => {
    return createPino(options);
  },
  inject: [LOGGER_MODULE_OPTIONS_TOKEN],
};
