import { Inject, Injectable, LoggerService, LogLevel, Scope } from '@nestjs/common';
import { Logger as PinoLogger } from 'pino';

import { LoggerLevel, PINO_TOKEN } from './types';
import { getTraceId } from './utils';

export interface LoggerOptions {
  isDevelopment: boolean;
  logLevel: LoggerLevel;
}

@Injectable({ scope: Scope.TRANSIENT })
export class Logger implements LoggerService {
  protected context?: string;

  constructor(@Inject(PINO_TOKEN) protected readonly pino: PinoLogger) {}

  setContext(context: string) {
    this.context = context;
  }

  debug(message: any, ...optionalParams: any[]): void {
    this.pino.debug(this.buildLogData('debug', message, ...optionalParams));
  }

  error(message: any, ...optionalParams: any[]): any {
    this.pino.error(this.buildLogData('error', message, ...optionalParams));
  }

  fatal(message: any, ...optionalParams: any[]): any {
    this.pino.fatal(this.buildLogData('fatal', message, ...optionalParams));
  }

  log(message: any, ...optionalParams: any[]) {
    this.pino.info(this.buildLogData('info', message, ...optionalParams));
  }

  info(message: any, ...optionalParams: any[]) {
    this.pino.info(this.buildLogData('info', message, ...optionalParams));
  }

  setLogLevels(levels: LogLevel[]) {
    return void levels;
  }

  warn(message: any, ...optionalParams: any[]) {
    this.pino.warn(this.buildLogData('warn', message, ...optionalParams));
  }

  verbose(message: any, ...optionalParams: any[]) {
    this.pino.trace(this.buildLogData('trace', message, ...optionalParams));
  }

  trace(message: any, ...optionalParams: any[]) {
    this.pino.trace(this.buildLogData('trace', message, ...optionalParams));
  }

  private buildLogData(level: LoggerLevel, message: any, ...optionalParams: any[]) {
    const objArg: Record<string, any> = { traceId: getTraceId() };

    let params: any[] = [];

    if (this.context) {
      objArg.context = this.context;
    } else if (optionalParams.length !== 0) {
      objArg.context = optionalParams[optionalParams.length - 1];
      params = optionalParams.slice(0, -1);
    }

    if (typeof message === 'object') {
      if (message instanceof Error) {
        objArg.err = message;
      } else {
        Object.assign(objArg, message);
      }
      return objArg;
    }

    if (this.isWrongExceptionsHandlerContract(level, message, params)) {
      objArg.err = new Error(message);
      objArg.err.stack = params[0];
      return objArg;
    }

    if (typeof message === 'string') {
      objArg.msg = message;
    }

    return objArg;
  }
  /**
   * Nestjs
   * - ExceptionsHandler
   * @see https://github.com/nestjs/nest/blob/35baf7a077bb972469097c5fea2f184b7babadfc/packages/core/exceptions/base-exception-filter.ts#L60-L63
   *
   * - ExceptionHandler
   * @see https://github.com/nestjs/nest/blob/99ee3fd99341bcddfa408d1604050a9571b19bc9/packages/core/errors/exception-handler.ts#L9
   *
   * - WsExceptionsHandler
   * @see https://github.com/nestjs/nest/blob/9d0551ff25c5085703bcebfa7ff3b6952869e794/packages/websockets/exceptions/base-ws-exception-filter.ts#L47-L50
   *
   * - RpcExceptionsHandler @see https://github.com/nestjs/nest/blob/9d0551ff25c5085703bcebfa7ff3b6952869e794/packages/microservices/exceptions/base-rpc-exception-filter.ts#L26-L30
   *
   * - all of them
   * @see https://github.com/search?l=TypeScript&q=org%3Anestjs+logger+error+stack&type=Code
   */
  private isWrongExceptionsHandlerContract(level: LoggerLevel, message: any, params: any[]): params is [string] {
    return (
      level === 'error' &&
      typeof message === 'string' &&
      params.length === 1 &&
      typeof params[0] === 'string' &&
      /\n\s*at /.test(params[0])
    );
  }
}
