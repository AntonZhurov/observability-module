import { DynamicModule, Inject, Provider } from '@nestjs/common';
import { HttpAdapterHost } from '@nestjs/core';

import { Logger } from './logger';
import { LoggerModule } from './logger/logger.module';
import { MetricsModule } from './metrics/metrics.module';
import { MetricsService } from './metrics/metrics.service';
import { ObservabilityModuleConfig, ObservabilityModuleConfigToken } from './observability.config.type';
import { setupObservability } from './setup';

export class ObservabilityModule {
  constructor(
    private httpAdapterHost: HttpAdapterHost,
    private logger: Logger,
    private metrics: MetricsService,
    @Inject(ObservabilityModuleConfigToken)
    private moduleOptions: ObservabilityModuleConfig,
  ) {
    setupObservability(this.httpAdapterHost.httpAdapter, this.logger, this.metrics, this.moduleOptions);
  }

  static forRoot(options: ObservabilityModuleConfig): DynamicModule {
    const modules: any[] = [MetricsModule];
    const exports: any[] = [MetricsModule, LoggerModule];
    const providers: Provider[] = [
      {
        provide: ObservabilityModuleConfigToken,
        useValue: options,
      },
    ];

    const { logger } = options;
    if (logger.forRoot) modules.push(LoggerModule.forRoot(logger.forRoot));
    if (logger.forRootAsync) modules.push(LoggerModule.forRootAsync(logger.forRootAsync));

    return {
      module: ObservabilityModule,
      global: true,
      imports: modules,
      providers: providers,
      exports: exports,
    };
  }
}
