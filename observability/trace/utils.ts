import * as opentelemetry from '@opentelemetry/api';

import { ObservabilityStorage } from '../observability.storage';

export const tracer = opentelemetry.trace.getTracer('smartway-backend');

export async function trace<TReturn>(
  name: string,
  cb: (span: opentelemetry.Span) => Promise<TReturn> | TReturn,
): Promise<TReturn> {
  return span(name, (span) => {
    return ObservabilityStorage.run(
      {
        traceId: span.spanContext().traceId,
        context: { type: 'custom' },
      },
      cb,
      span,
    );
  });
}

export async function span<TReturn>(
  name: string,
  cb: (span: opentelemetry.Span) => Promise<TReturn> | TReturn,
): Promise<TReturn> {
  return tracer.startActiveSpan(name, async (span) => {
    try {
      const result = await cb(span);
      span.setStatus({
        code: opentelemetry.SpanStatusCode.OK,
      });
      return result;
    } catch (err: any) {
      span.setStatus({
        code: opentelemetry.SpanStatusCode.ERROR,
        message: err.message,
      });
      throw err;
    } finally {
      span.end();
    }
  });
}

// export async function span<TReturn>(
//   name: string,
//   cb: (span: opentelemetry.Span) => Promise<TReturn> | TReturn,
// ): Promise<TReturn> {
//   const store = ObservabilityStorage.getStore();
//   const spanContext = store?.spanContext;
//   const span = tracer.startSpan(name, undefined, spanContext);
//   if (store) {
//     store.spanContext = opentelemetry.trace.setSpan(store.spanContext, span);
//   }

//   try {
//     const result = await cb(span);
//     span.setStatus({
//       code: opentelemetry.SpanStatusCode.OK,
//     });
//     return result;
//   } catch (err: any) {
//     span.setStatus({
//       code: opentelemetry.SpanStatusCode.ERROR,
//       message: err.message,
//     });
//     throw err;
//   } finally {
//     span.end();
//   }
// }
