import { CompositePropagator, W3CBaggagePropagator, W3CTraceContextPropagator } from '@opentelemetry/core';
import { OTLPTraceExporter } from '@opentelemetry/exporter-trace-otlp-http';
import { Resource } from '@opentelemetry/resources';
import { NodeSDK } from '@opentelemetry/sdk-node';
import { BatchSpanProcessor } from '@opentelemetry/sdk-trace-base';
import {
  SEMRESATTRS_DEPLOYMENT_ENVIRONMENT,
  SEMRESATTRS_SERVICE_NAME,
  SEMRESATTRS_SERVICE_VERSION,
} from '@opentelemetry/semantic-conventions';

let sdk: NodeSDK | null = null;

(() => {
  const env = process.env.NODE_ENV;
  const tracingEnablend = process.env.OTLP_TRACE_ENABLED ?? false;
  const traceExportEndpoint = process.env.OTLP_TRACE_EXPORT_ENDPOINT;

  if (!tracingEnablend) return null;

  const traceExporter = new OTLPTraceExporter({
    url: `${traceExportEndpoint}/v1/traces`,
  });

  sdk = new NodeSDK({
    resource: new Resource({
      [SEMRESATTRS_SERVICE_NAME]: 'your-app-name',
      [SEMRESATTRS_SERVICE_VERSION]: '0.0.1',
      [SEMRESATTRS_DEPLOYMENT_ENVIRONMENT]: env,
    }),
    spanProcessors: [new BatchSpanProcessor(traceExporter)],
    traceExporter: traceExporter,
    textMapPropagator: new CompositePropagator({
      propagators: [new W3CBaggagePropagator(), new W3CTraceContextPropagator()],
    }),
  });
  sdk.start();
})();
