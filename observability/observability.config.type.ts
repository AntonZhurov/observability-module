import { LoggerModuleAsyncOptions, LoggerModuleOptions } from './logger/types';

export interface ObservabilityModuleLoggerConfig {
  forRoot?: LoggerModuleOptions;
  forRootAsync?: LoggerModuleAsyncOptions;
}

export interface ObservabilityModuleGraphQlConfig {
  url: string;
}

export interface ObservabilityModuleTraicingConfig {
  urlBlackListRegexp: RegExp[];
}

export interface ObservabilityModuleConfig {
  logger: ObservabilityModuleLoggerConfig;
  graphql?: ObservabilityModuleGraphQlConfig;
  traicing?: ObservabilityModuleTraicingConfig;
}

export const ObservabilityModuleConfigToken = Symbol('ObservabilityModuleConfig');
