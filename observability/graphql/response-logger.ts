import * as http from 'node:http';

import { buildOperationName } from './common';
import { GraphQlRequestObservabilityContext } from './observability.context';
import { GraphQlOperationDetails } from './utils';

interface BuildPayload {
  req: http.IncomingMessage;
  res: http.ServerResponse;
  err?: Error;
  responseTime?: number;
  operationDetails?: GraphQlOperationDetails;
}

const buildSuccessfulLog = ({ req, res, responseTime, operationDetails }: BuildPayload) => ({
  msg: 'Request completed',
  req: {
    id: req['id'],
    url: req.url,
    method: req.method,
    headers: req.headers,
  },
  graphql: {
    name: buildOperationName(operationDetails || null),
  },
  res: {
    status: res.statusCode,
    headers: res.getHeaders(),
  },
  responseTime: responseTime,
});

const buildFailedLog = ({ req, res, err, responseTime, operationDetails }: BuildPayload) => ({
  msg: 'Request failed',
  req: {
    id: req['id'],
    url: req.url,
    method: req.method,
    headers: req.headers,
  },
  graphql: {
    name: buildOperationName(operationDetails || null),
  },
  res: {
    status: res.statusCode,
    headers: res.getHeaders(),
  },
  err: err,
  responseTime: responseTime,
});

export const logGraphQlResponse = (context: GraphQlRequestObservabilityContext, err?: any) => {
  const { res, req, logger, error, graphqlOperationDetails } = context;
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  //@ts-ignore
  const responseTime = Date.now() - res['startTime'];
  if (error) {
    logger.error(
      buildFailedLog({
        req: req,
        res: res,
        err: error ?? err,
        responseTime,
        operationDetails: graphqlOperationDetails,
      }),
    );
  } else {
    logger.log(
      buildSuccessfulLog({
        req: req,
        res: res,
        responseTime,
        operationDetails: graphqlOperationDetails,
      }),
    );
  }
};
