import {
  BaseContext,
  GraphQLRequestContext,
  GraphQLRequestContextDidResolveOperation,
  GraphQLRequestContextWillSendResponse,
  GraphQLRequestListener,
} from '@apollo/server';

import { Logger } from '../logger';
import { ObservabilityStorage } from '../observability.storage';
import { updateOperationSegmentName } from './common';

export type HookOptions = {
  logger: Logger;
};

export async function requestDidStart(
  options: HookOptions,
  context: GraphQLRequestContext<BaseContext>,
): Promise<GraphQLRequestListener<BaseContext> | void> {
  return {
    didResolveOperation: didResolveOperation.bind(null, options),
  };
}

async function didResolveOperation(
  options: HookOptions,
  context: GraphQLRequestContextDidResolveOperation<BaseContext>,
) {
  const store = ObservabilityStorage.getStore();
  if (!store || store.context.type !== 'graphql') return;
  updateOperationSegmentName(context, store.context);
}
