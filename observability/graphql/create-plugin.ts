import { ApolloServerPlugin } from '@apollo/server';

import { HookOptions, requestDidStart } from './hooks';

export function createGraphQlObservabilityPlugin(options: HookOptions): ApolloServerPlugin {
  return {
    requestDidStart: requestDidStart.bind(null, options),
  };
}
