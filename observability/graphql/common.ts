import { BaseContext, GraphQLRequestContextDidResolveOperation } from '@apollo/server';

import { ObservabilityStorage } from '../observability.storage';
import { GraphQlRequestObservabilityContext } from './observability.context';
import { getOperationDetails, GraphQlOperationDetails } from './utils';

const ANON_PLACEHOLDER = '<anonymous>';
const CATEGORY = 'GraphQL';
const FRAMEWORK = 'ApolloServer';
const OPERATION_PREFIX = CATEGORY + '/operation/' + FRAMEWORK;
const RESOLVE_PREFIX = CATEGORY + '/resolve/' + FRAMEWORK;
const ARG_PREFIX = `${CATEGORY}/arg/${FRAMEWORK}`;
const FIELD_PREFIX = `${CATEGORY}/field/${FRAMEWORK}`;
const BATCH_PREFIX = 'batch';
const DEFAULT_OPERATION_NAME = `${OPERATION_PREFIX}/<unknown>`;

const FIELD_NAME_ATTR = 'graphql.field.name';
const PARENT_TYPE_ATTR = 'graphql.field.parentType';
const RETURN_TYPE_ATTR = 'graphql.field.returnType';
const FIELD_PATH_ATTR = 'graphql.field.path';
const OPERATION_TYPE_ATTR = 'graphql.operation.type';
const OPERATION_NAME_ATTR = 'graphql.operation.name';
const OPERATION_QUERY_ATTR = 'graphql.operation.query';

export function buildOperationName(operationDetails: GraphQlOperationDetails) {
  if (!operationDetails) return ANON_PLACEHOLDER;
  const { operationName, operationType, deepestUniquePath, cleanedQuery } = operationDetails;
  const formattedName = operationName || ANON_PLACEHOLDER;
  let formattedOperation = `${operationType}/${formattedName}`;

  // Certain requests, such as introspection, won't hit any resolvers
  if (deepestUniquePath) {
    formattedOperation += `/${deepestUniquePath}`;
  }

  const segmentName = formattedOperation;
  return `${OPERATION_PREFIX}/${segmentName}`;
}

export function updateOperationSegmentName(
  context: GraphQLRequestContextDidResolveOperation<BaseContext>,
  observabilityContext: GraphQlRequestObservabilityContext,
) {
  const operationDetails = getOperationDetails(context);
  const store = ObservabilityStorage.getStore();
  if (store && store.context.type === 'graphql') {
    store.context.graphqlOperationDetails = operationDetails;
  }
  if (operationDetails) {
    const requestSpan = observabilityContext.requestSpan;
    const { operationName, operationType, deepestUniquePath, cleanedQuery } = operationDetails;

    requestSpan.setAttribute(OPERATION_QUERY_ATTR, cleanedQuery);

    requestSpan.setAttribute(OPERATION_TYPE_ATTR, operationType);

    observabilityContext.requestSpan = requestSpan.updateName(buildOperationName(operationDetails));
    return true;
  }

  return false;
}
