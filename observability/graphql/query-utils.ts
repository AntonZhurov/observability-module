const OBFUSCATION_STR = '***';

export function cleanQuery(query: any, argLocations: any) {
  let cleanedQuery = query;
  let offset = 0;

  argLocations.forEach((loc: any) => {
    cleanedQuery = cleanedQuery.slice(0, loc.start - offset) + OBFUSCATION_STR + cleanedQuery.slice(loc.end - offset);

    offset = loc.end - loc.start - OBFUSCATION_STR.length;
  });

  return cleanedQuery;
}
