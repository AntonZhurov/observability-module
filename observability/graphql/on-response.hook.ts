import { OnResponseHookCb } from '../utils/response-handler';
import { buildOperationName } from './common';
import { logGraphQlResponse } from './response-logger';

export const OnResponseGraphQlHook: OnResponseHookCb = (store) => {
  if (store.context.type !== 'graphql') return;

  const { route, res, graphqlOperationDetails } = store.context;

  store.context.collectResponseTimeCb({
    method: route.method,
    path: buildOperationName(graphqlOperationDetails || null),
    status: res.statusCode,
  });
  store.context.requestSpan.end();

  logGraphQlResponse(store.context);
};
