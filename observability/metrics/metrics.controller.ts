import { Controller, Get, Res } from '@nestjs/common';
import { ApiExcludeController } from '@nestjs/swagger';
import { FastifyReply } from 'fastify';

import { MetricsService } from './metrics.service';

@ApiExcludeController()
@Controller('metrics')
export class MetricsController {
  constructor(private service: MetricsService) {}

  @Get()
  async metrics(@Res() reply: FastifyReply) {
    const metrics = await this.service.collect();
    return void reply.header('Content-Type', this.service.getContentType()).send(metrics);
  }
}
