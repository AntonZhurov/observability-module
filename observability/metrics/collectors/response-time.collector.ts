import { Histogram, Registry } from 'prom-client';

export type ResponseTimeCollector = ReturnType<typeof createResponseTimeCollector>;

export function createResponseTimeCollector(registry?: Registry) {
  return new Histogram({
    name: 'nodejs_response_time',
    help: 'Endpoints response time in sec',
    registers: registry ? [registry] : undefined,
    labelNames: ['method', 'path', 'status'],
    buckets: [0.8, 1, 2, 3, 5],
  });
}
