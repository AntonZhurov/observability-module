import { Module } from '@nestjs/common';

import { MetricsController } from './metrics.controller';
import { MetricsService } from './metrics.service';
import { RegistryProvider } from './registry.provider';

@Module({
  controllers: [MetricsController],
  providers: [MetricsService, RegistryProvider],
  exports: [MetricsService],
})
export class MetricsModule {}
