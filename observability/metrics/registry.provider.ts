import { Provider } from '@nestjs/common';
import { Registry } from 'prom-client';

export const RegistryProvider: Provider<Registry> = {
  provide: Registry,
  useFactory: () => {
    const registry = new Registry();
    registry.setDefaultLabels({
      app: 'your-app-name',
      env: 'development',
    });
    return registry;
  },
  inject: [],
};
