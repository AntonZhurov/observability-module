import { Injectable } from '@nestjs/common';
import { collectDefaultMetrics, Registry } from 'prom-client';

import { createResponseTimeCollector } from './collectors/response-time.collector';

@Injectable()
export class MetricsService {
  public readonly responseTimeCollector = createResponseTimeCollector(this.registry);

  constructor(private registry: Registry) {
    collectDefaultMetrics({
      register: this.registry,
      gcDurationBuckets: [0.1, 0.2, 0.3],
    });
  }

  async collect() {
    return this.registry.metrics();
  }

  getContentType() {
    return this.registry.contentType;
  }
}
