import { AsyncLocalStorage } from 'node:async_hooks';

import { GraphQlRequestObservabilityContext } from './graphql/observability.context';
import { HttpRequestObservabilityContext } from './http/observability.context';
import { CustomObservabilityContext } from './trace/observability.context';

export type ObservabilityStoreContext =
  | HttpRequestObservabilityContext
  | GraphQlRequestObservabilityContext
  | CustomObservabilityContext;

export type ObservabilityStoreContextType = ObservabilityStoreContext['type'];

export interface ObservabilityStore {
  traceId: string;
  context: ObservabilityStoreContext;
}

export const ObservabilityStorage = new AsyncLocalStorage<ObservabilityStore>();
