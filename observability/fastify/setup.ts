import { FastifyAdapter } from '@nestjs/platform-fastify';

import { Logger } from '../logger';
import { MetricsService } from '../metrics/metrics.service';
import { ObservabilityModuleConfig } from '../observability.config.type';
import { createOnRequestHook, createOnResponseHook } from './fastify-hooks';

export function setupFastifyObservability(
  httpAdapter: FastifyAdapter,
  logger: Logger,
  metrics: MetricsService,
  config: ObservabilityModuleConfig,
) {
  httpAdapter
    .getInstance()
    .addHook('onRequest', createOnRequestHook(logger, metrics, config))
    .addHook('onResponse', createOnResponseHook());
}
