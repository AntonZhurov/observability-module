import { onRequestHookHandler, onResponseHookHandler } from 'fastify';

import { Logger } from '../logger';
import { MetricsService } from '../metrics/metrics.service';
import { ObservabilityModuleConfig } from '../observability.config.type';
import { ObservabilityStorage } from '../observability.storage';
import { tracer } from '../trace/utils';
import { createObservabilityStore } from '../utils/observability-store.factory';
import { HandleResponse } from '../utils/response-handler';

export function createOnRequestHook(
  logger: Logger,
  metrics: MetricsService,
  config: ObservabilityModuleConfig,
): onRequestHookHandler {
  const routeBlackListRegexp = config.traicing?.urlBlackListRegexp || [];

  return (request, reply, done) => {
    const { method, url } = request.routeOptions;

    if (url && routeBlackListRegexp.some((reg) => reg.test(url))) return void done();

    const res = reply.raw;
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    //@ts-ignore
    res['startTime'] = res['startTime'] || Date.now();

    tracer.startActiveSpan(`${method} ${url}`, (span) => {
      return ObservabilityStorage.run(
        createObservabilityStore(config, {
          request: request,
          reply: reply,
          span: span,
          logger: logger,
          collectResponseTimeCb: metrics.responseTimeCollector.startTimer(),
        }),
        done,
      );
    });
  };
}

export function createOnResponseHook(): onResponseHookHandler {
  return (request, reply, done) => {
    const store = ObservabilityStorage.getStore();
    if (!store) return void done();

    return void HandleResponse(store, done);
  };
}
