# Observability module

Модуль предоставляет функционал для трейсинга, логгирования и сбора метрик. Каждому проекту нужны системные (дефолтные) и кастомные метрики, по этой причине модуль предоставляется как исходный код, чтобы каждый мог самостоятельно и максимально гибко настроить необходимые "кастомные" метрики.

## Важно
Чтобы работало логирование запросов и трейсинг запросов нужно чтобы ваш nestjs http adapter был fastify. Во первых использовать express это мовитон, во вторых fastify сильно быстрей, в третьих  fastify предоставляет хуки, которые меньше влияют на перфоманс, чем мидлвары в express.

В будущем возможно добавим поддержку для express http adapter.

Сейчас если у вас адаптер не fastify, то у вас не появиться авоматический трейсинг всех запросов и логгирование всех запросов

## Установка

Модуль стремиться быть максимально гибким, поэтому он устанавливается не как npm пакет, а как исходный код. Из-за этого установка происходит в несколько шагов.

1) Копируем в проект папку `observability`

2) Устанавливаем необходимые пакеты.
Необходимо установить пакеты для трейсинга, сбора метрик и логгирования.

Базовые пакеты
```sh
yarn add @opentelemetry/api @opentelemetry/exporter-trace-otlp-http @opentelemetry/resources @opentelemetry/sdk-node @opentelemetry/sdk-trace-base @opentelemetry/semantic-conventions dotenv prom-client pino
```
Пакет для красивого отображения логов в dev режиме

```sh
yarn add -D pino-pretty
```

Если вам необходимо трейсить и логировать запросы graphql, сейчас поддерживается только apollo server
```sh
yarn add @nestjs/graphql graphql @apollo/server
```
3) Для того чтобы корректно активировать трейсинг нужно самой первой строчкой в main файле вставить код представленный ниже. При необходимости поменяйте путь до `observability/trace/setup` скрипта. Если в трейсинге нету необходимости, то просто игнорируйте этот шаг.

```ts
import 'dotenv/config';
import './observability/trace/setup';
```
4) В файле `observability/metrics/registry.provider.ts` меняем AppName на релевантное вам имя, если неоходимо, то делаем настройку registry используя ваш ConfigService. Эти данные будут необходимы вам для вывода метрик в grafana.
```ts
const AppName = 'your-app-name'

export const RegistryProvider: Provider<Registry> = {
  provide: Registry,
  useFactory: () => {
    const registry = new Registry();
    registry.setDefaultLabels({
      app: AppName,
      env: process.env.NODE_ENV,
    });
    return registry;
  },
  inject: [],
};
```

В файле `observability/trace/setup` настраиваем 'your-app-name' и версию

```ts
sdk = new NodeSDK({
    resource: new Resource({
      [SEMRESATTRS_SERVICE_NAME]: 'your-app-name',
      [SEMRESATTRS_SERVICE_VERSION]: '0.0.1',
      [SEMRESATTRS_DEPLOYMENT_ENVIRONMENT]: env,
    }),
    spanProcessors: [new BatchSpanProcessor(traceExporter)],
    traceExporter: traceExporter,
    textMapPropagator: new CompositePropagator({
      propagators: [new W3CBaggagePropagator(), new W3CTraceContextPropagator()],
    }),
  });
```

5) Добавляем в AppModule

```ts
@Module({
  imports: [
    ObservabilityModule.forRoot({
      // Конфиг для логгера, так же можно использовать forRootAsync, если необходимо использовать di
      logger: {
        forRoot: {
          isDevelopment: true,
          logLevel: 'trace',
        },
      },
      graphql: {
        // Если есть graphql необходимо указать на каком эндпоинте весит обработчик
        url: '/graphql',
      },
      traicing: {
        // Можем отключить трейсинг для определенных эндпоинтов
        urlBlackListRegexp: [/\/health(.*)/, /\/metrics/, /\/api(.*)/],
      },
    })
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
```

6) Если у вас есть graphql и вы хотите трейсить эти запросы, то вам необходимо установить плагин из папки `graphql`.

```ts
import { ApolloDriverConfig } from '@nestjs/apollo';
import { Injectable } from '@nestjs/common';
import { GqlOptionsFactory } from '@nestjs/graphql';
import { createGraphQlObservabilityPlugin } from 'src/observability/graphql/create-plugin';
import { Logger } from 'src/observability/logger';

@Injectable()
export class GraphQLConfigService implements GqlOptionsFactory {
  constructor(private readonly logger: Logger) {}

  createGqlOptions(): ApolloDriverConfig {
    return {
      playground: true,
      plugins: [createGraphQlObservabilityPlugin({ logger: this.logger }),],
    };
  }
}
```

```ts
@Module({
  imports: [
    GraphQLModule.forRootAsync({
      driver: ApolloDriver,
      useClass: GraphQLConfigService,
    }),
  ],
})
export class AppModule {}
```

7) Добавляем необходимые энвы
 - `NODE_ENV: string`
 - `OTLP_TRACE_ENABLED: boolean`
 - `OTLP_TRACE_EXPORT_ENDPOINT: string` - адрес вашего коллектора трейсов

Версии пакетов на которых написан модуль
```json
{
  "dependencies": {
    "@nestjs/core": "^10.3.3",
    "@nestjs/platform-fastify": "^10.3.3",
    "@opentelemetry/api": "^1.8.0",
    "@opentelemetry/exporter-trace-otlp-http": "^0.51.0",
    "@opentelemetry/resources": "^1.24.0",
    "@opentelemetry/sdk-node": "^0.51.0",
    "@opentelemetry/sdk-trace-base": "^1.24.0",
    "@opentelemetry/semantic-conventions": "^1.24.0",
    "prom-client": "^15.1.2",
    "dotenv": "^16.4.5",
    "pino": "^9.1.0",
    "graphql": "^16.8.1",
    "@nestjs/graphql": "^12.0.9",
    "@apollo/server": "^4.9.4",
  },
  "devDependencies": {
    "pino-pretty": "^11.0.0",
  }
}
```

## Observability module

Собирает в себе все три модуля для Observability.

В конструкторе запускает настройку логгирования запросов, сбора метрики responseTime и трейсинга запросов

Чтобы подробней разобраться в том, что настраивается изучите исходный код. 

```ts
export class ObservabilityModule {
  constructor(
    private httpAdapterHost: HttpAdapterHost,
    private logger: Logger,
    private metrics: MetricsService,
  ) {
    // Здесь происходит настройка
    setupObservability(this.httpAdapterHost.httpAdapter, this.logger, this.metrics);
  }
}
```

## Подмодули
- [Logger](./docs/logger.md)
- [Metrics](./docs/metrics.md)
- [Traicing](./docs/trace.md)
- [Request observability](./docs/request-observability.md)